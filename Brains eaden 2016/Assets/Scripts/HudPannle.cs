﻿using UnityEngine;
using UnityEngine.UI;

public class HudPannle : MonoBehaviour {

    [SerializeField]
    public Text DyistenceCounter;
    Game gameManager;

    void Start()
    {
        gameManager = GameObject.Find("Main Camera").GetComponent<Game>();
    }
	// Update is called once per frame
	void Update ()
    {
        DyistenceCounter.text = "" + (int) Game.TotalDistance + " M";
	}

    void PauseButton()
    {
        gameManager.PauseGame();
    }
}
