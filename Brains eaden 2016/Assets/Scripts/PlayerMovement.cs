﻿using UnityEngine;
using System.Collections;
public class PlayerMovement : MonoBehaviour
{

    public float MovementSpeed;
    public float FallSpeed;
    public float stunTime = 0;
    public float DefaultDashSpeed;
    float DashSpeed;
    float a = 0;
    bool coroutineRunning = false;
    float action = 0;
    float tapDelay = 0;
    GameObject gameCamera;
    bool stuned = false;
    bool dashStun = false;
    float stunTimer = 0;
    Vector3 dashEnd = Vector3.zero;
    bool isDashing = false;
    Vector3 spawnLocation;
    IEnumerator takeAction = null;
    AudioScript audio;


    // One Finger Touch variable
    Vector2 touch; //Stores the details of the touch like position

    //Swipe variables
    public float maxTime; //The max time a swipe can be 
    public float minSwipeDist; //How small a movement is counted as a swipe
    int swipeI = 0; //Used for geting swipe direction

    float startTime; //Stores the start time of the swipe
    float endTime; //Stores the end time of the swipe

    Vector3 startPos; //Start position of the swipe
    Vector3 endPos; //End position of the swipe
    float swipeDistance; //Distance swiped
    float swipeTime; //The amount of time the swipe was

    Game gameManager;

    public Animator Anim;

    void Start()
    {
        DashSpeed = DefaultDashSpeed;      
        audio = GameObject.Find("Audio").GetComponent<AudioScript>();
        Anim.Play("Idle");
        spawnLocation = transform.position;
        gameCamera = GameObject.Find("Main Camera");
        gameManager = gameCamera.GetComponent<Game>();
    }
    // Update is called once per frame
    Transform fall;
    void Update()
    {
        if (stunTimer >= stunTime)
            dashStun = false;
        tapDelay += Time.deltaTime;
        if (transform.position.y > gameCamera.transform.position.y + 6 || transform.position.y < gameCamera.transform.position.y - 6)
        {
            audio.playScreamClip();
            coroutineRunning = false;
            gameManager.EndGame();
            gameObject.SetActive(false);
        }
        fall = fallCheck();
        if (fall)
        {
            if (fall.gameObject.tag.Equals("stun"))
            {
                Anim.Play("Shocked");
                stuned = true;
                audio.playSparkClip();
            }
            else
                stuned = false;
            fall = null;
        }
        if (dashStun)
        {
            Anim.Play("Shocked");
            stunTimer += Time.deltaTime;
        }
        else
        {
            if (!stuned && gameObject.activeSelf)
            {
                if (Input.touchCount > 0)
                {
                    touch = Input.GetTouch(0).position; //Stores the position of a one finger touch

                    Touch touchSwipe = Input.GetTouch(0); //Stores all the information of a one finger touch

                    if (touchSwipe.phase == TouchPhase.Began) //If the swipe has begun store the time and position
                    {
                        startTime = Time.time;
                        startPos = touchSwipe.position;
                    }
                    else if (touchSwipe.phase == TouchPhase.Ended)
                    {
                        /*If the swipe has ended store the time it ends and the position is ends
                         Then calculate the total swipe distance 
                         Then calculate the total swipe time*/

                        endTime = Time.time;
                        endPos = touchSwipe.position;

                        swipeDistance = (endPos - startPos).magnitude;
                        swipeTime = endTime - startTime;

                        /*If the swipe time is less than the max allowed time of a swipe AND
                         the swipe distance is greater than the minimum allowed swipe distance then 
                         call the swipe method

                        If it's not a swipe then check if the touch is on the right or left of the screen and 
                        make the character move by 1
                         */


                        if (swipeTime < 0.3 && swipeDistance > minSwipeDist)
                        {
                            if (swipeTime < 0.15)
                            {
                                DashSpeed += swipeTime * 10;
                            }
                            else
                            {
                                DashSpeed *= swipeTime;
                            }
                            swipe();
                        }
                        else
                        {
                            if (tapDelay >= 0.15)
                            {
                                if (touch.x < (Screen.width / 2))
                                {
                                    action = -1;

                                }
                                if (touch.x >= (Screen.width / 2))
                                {
                                    action = 1;
                                }
                                tapDelay = 0;
                            }
                            else
                            {
                                StopAllCoroutines();
                                coroutineRunning = false;
                            }
                        }

                    }
                }
                if (!coroutineRunning)
                {
                    if (swipeI != 0)
                    {
                        if (swipeI == 1)
                        {
                            dashEnd = dashCheck(Vector3.right);
                            if (dashEnd != Vector3.zero)
                            {
                                audio.playDashClip();
                                Anim.Play("DashRight");
                                isDashing = true;
                                takeAction = Move(dashEnd + new Vector3(-0.5f, 0, 0), DashSpeed);
                                StartCoroutine(takeAction);
                            }

                        }
                        else
                        {
                            if (swipeI == -1)
                            {
                                dashEnd = dashCheck(Vector3.left);
                                if (dashEnd != Vector3.zero)
                                {
                                    Anim.Play("DashLeft");
                                    audio.playDashClip();
                                    isDashing = true;
                                    takeAction = Move(dashEnd + new Vector3(0.5f, 0, 0), DashSpeed);
                                    StartCoroutine(takeAction);
                                }
                            }
                            else
                            {
                                if (swipeI == -2)
                                {
                                    GameObject platform = dropCheck();
                                    if (platform != null)
                                    {
                                        audio.playBreakClip();
                                        platform.SetActive(false);
                                        fall = fallCheck();
                                        if (fall != null && Vector3.Distance(transform.position, fall.position) > 1)
                                        {
                                            Anim.Play("Falling");
                                            audio.playBreakClip();
                                            takeAction = Move(fall.position + new Vector3(0, 1), FallSpeed);
                                            StartCoroutine(takeAction);
                                            fall = null;
                                        }
                                        platform.SetActive(true);
                                    }
                                }
                            }
                        }

                    }
                    else
                    {
                        if (action != 0)
                        {
                            Vector3 direction = new Vector3(action, 0, 0);
                            if (moveCheck(direction))
                            {
                                if (action > 0)
                                    Anim.Play("runRight");
                                else
                                    Anim.Play("runLeft");
                                takeAction = Move(transform.position + direction, MovementSpeed);
                                StartCoroutine(takeAction);

                            }
                            action = 0;
                        }
                        else
                        {
                            fall = fallCheck();
                            if (fall != null && Vector3.Distance(transform.position, fall.position) > 1.5)
                            {
                                print("a");
                                StopAllCoroutines();
                                coroutineRunning = false;
                                Anim.Play("Falling");
                                takeAction = Move(fall.position + new Vector3(0, 1), FallSpeed);
                                StartCoroutine(takeAction);
                                fall = null;
                                
                            }
                        }
                    }
                }
            }
        }
    }

    IEnumerator Move(Vector3 target, float speed)
    {
        coroutineRunning = true;
        while (Vector3.Distance(transform.position, target) > 0.01f)
        {
            transform.position = Vector3.Lerp(transform.position, target, a * Time.deltaTime);
            a += speed;
            if (isDashing)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, Vector3.right, out hit) && swipeI == 1)
                {
                    if (Vector3.Distance(transform.position, hit.transform.position) <= 2 && hit.collider.tag.Equals("dashStun"))
                    {
                        dashStun = true;
                        stunTimer = 0;
                    }
                }
                else
                {
                    if  (Physics.Raycast(transform.position, Vector3.left, out hit) && swipeI == -1)
                    {
                        if (Vector3.Distance(transform.position, hit.transform.position) <= 2 && hit.collider.tag.Equals("dashStun"))
                        {
                            dashStun = true;
                            stunTimer = 0;
                        }
                    }
                }
            }
            yield return null;
        }
        a = 0;
        isDashing = false;
        DashSpeed = DefaultDashSpeed;
        swipeI = 0;
        coroutineRunning = false;
    }

    Transform fallCheck()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
            return hit.transform;
        return null;
    }

    bool moveCheck(Vector3 direction)
    {
        if (Physics.Raycast(transform.position, direction, 1))
            return false;
        return true;
    }

    Vector3 dashCheck(Vector3 direction)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, direction, out hit))
            return hit.point;
        return Vector3.zero;
    }

    GameObject dropCheck()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit))
            if (hit.collider.tag.Equals("drop"))
                return hit.collider.gameObject;
        return null;
    }

    public void respawn()
    {
        StopAllCoroutines();
        coroutineRunning = false;
        transform.position = spawnLocation;
        gameObject.SetActive(true);
    }
    void swipe()
    {
        Vector2 distance = endPos - startPos;
        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {
            if (distance.x < 0)
            {
                swipeI = -1;
            }

            if (distance.x > 0)
            {
                swipeI = 1;
            }

        }
        else
        {
            if (Mathf.Abs(distance.x) < Mathf.Abs(distance.y))
            {
                if (distance.y < 0)
                    swipeI = -2;
            }
        }
    }
}
