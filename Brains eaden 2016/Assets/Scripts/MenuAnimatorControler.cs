﻿using UnityEngine;
using System.Collections;

public class MenuAnimatorControler : MonoBehaviour {

    void SetScreen(Animator Screen, bool IsShowing)
    {
        Screen.SetBool("IsShowing", IsShowing);
    }

}
