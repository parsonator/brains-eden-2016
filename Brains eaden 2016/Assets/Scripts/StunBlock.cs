﻿using UnityEngine;
using System.Collections;

public class StunBlock : MonoBehaviour {

    public float speed = 0;
    public MeshRenderer Effct;
    float timePassed=0;
    bool stun = false;
    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed > speed)
        {
            if (!stun)
            {
                gameObject.tag = ("stun");
                stun = true;
                Effct.enabled = true;
            }
            else
            {
                gameObject.tag = ("Untagged");
                stun = false;
                Effct.enabled = false;

            }
            timePassed = 0;
        }
    }
}
