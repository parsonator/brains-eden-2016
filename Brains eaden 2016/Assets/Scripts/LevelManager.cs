﻿using UnityEngine;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {
    public GameObject startChunk;


    [Header("Chunk Arrays")]
    public GameObject[] EasyChunks;
    public float DifficaltyTimer_Medium = 30;
    public GameObject[] MediumChunks ;
    public float DifficaltyTimer_Hard= 70;
    public GameObject[] HardChunks;


    [Header("Chunks"),Space()]
    public
    List<GameObject> spawnedChunks = new List<GameObject>();
    int spawnLocation = -10;
    // Use this for initialization
    void Start()
    {

        SetUp();

    }

    void Update()
    {

        if (transform.position.y <= spawnLocation)
        {


            /*
           spawnLocation -= 11;
           spawnedChunks[0].SetActive(false);
           spawnedChunks.RemoveAt(0);
           int i= Random.Range(0, EasyChunks.Length);
           while (EasyChunks[i].activeSelf == true)
               i = Random.Range(0, EasyChunks.Length);
           spawnedChunks.Add(EasyChunks[i]);
           spawnedChunks[2].SetActive(true);
           spawnedChunks[2].transform.position = new Vector3(0, spawnLocation);

    */
            if (Game.TimePlayed < DifficaltyTimer_Medium)
            {
                SpawnChunk(EasyChunks);
                Debug.Log("Easy");
            }
            else if (Game.TimePlayed > DifficaltyTimer_Medium && Game.TimePlayed < DifficaltyTimer_Hard )
            {
                SpawnChunk(MediumChunks);
                Debug.Log("Med");

            }
            else if (Game.TimePlayed >= DifficaltyTimer_Hard)
            {
                SpawnChunk(HardChunks);
                Debug.Log("Hard");

            }

        }
    }

    void SpawnChunk(GameObject[] ChunkList)
    {

        spawnLocation -= 11;
        spawnedChunks[0].SetActive(false);
        spawnedChunks.RemoveAt(0);
        int i = Random.Range(0, ChunkList.Length);
        while (ChunkList[i].activeSelf == true)
            i = Random.Range(0, ChunkList.Length);
        spawnedChunks.Add(ChunkList[i]);
        spawnedChunks[2].SetActive(true);
        spawnedChunks[2].transform.position = new Vector3(0, spawnLocation);

    }

    public void SetUp()
    {
        spawnLocation = -10;
        spawnedChunks.Add(EasyChunks[Random.Range(0, EasyChunks.Length)]);
        spawnedChunks[0].transform.position = new Vector3(0, spawnLocation);
        spawnedChunks[0].SetActive(true);
        spawnLocation -= 11;
        int i = Random.Range(0, EasyChunks.Length);
        while (EasyChunks[i].activeSelf == true)
            i = Random.Range(0, EasyChunks.Length);
        spawnedChunks.Add(EasyChunks[i]);
        spawnedChunks[1].SetActive(true);
        spawnedChunks[1].transform.position = new Vector3(0, spawnLocation);
    }

    public void clear()
    {
        while(spawnedChunks.Count>0)
        {
            spawnedChunks[0].SetActive(false);
            spawnedChunks.RemoveAt(0);
        }
    }
}
