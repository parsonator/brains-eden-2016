﻿using UnityEngine;
using UnityEngine.UI;
[ExecuteInEditMode]
public class ChunkDebugHelper : MonoBehaviour {
    [SerializeField]
    Text NameText;

	// Use this for initialization
	void Start () {
        NameText.text = "| "+gameObject.name;
	}
}
