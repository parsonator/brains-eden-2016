﻿using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour {

    public static GameState currentState;

    public static float TotalDistance;
    public static float TimePlayed;

    public static bool isLeftPlayerDead, isRightPlayerDead;

    public GameObject LeftPlayer, RightPlayer;
    public GameObject GameOverScreen, PauseScreen;
    public Text GameOverText;
    public Text Highscore;
    public GameObject controlsPanel;

    void Start()
    {
        ControlsGame();
    }

    void Update()
    {
        TimePlayed += Time.deltaTime;

        switch (currentState)
        {
            case GameState.Playing:
                TotalDistance = -CalculateDistanceTraveled();

                break;
            case GameState.Paused:

                break;
            case GameState.Dead:
                GameOverScreen.SetActive(true);
                GameOverText.text = "You made it to " + (int)TotalDistance + " metres"; 
                break;

            case GameState.Controls:
                

                break;

            default:
                break;
        }

    }

    public static void StartGame()
    {
        TimePlayed = 0;
    }

    /// <summary>
    /// Restarts the game
    /// </summary>
    public  void RestartGame()
    {
        transform.position = new Vector3(0, 1, -10);
        TimePlayed = 0;
        TotalDistance = 0;
        GameOverScreen.SetActive(false);
        PauseScreen.SetActive(false);
        controlsPanel.SetActive(true);
        LeftPlayer.GetComponent<PlayerMovement>().respawn();
        RightPlayer.GetComponent<PlayerMovement>().respawn();
        gameObject.GetComponent<LevelManager>().SetUp();
        LeftPlayer.GetComponent<PlayerMovement>().enabled = true;
        RightPlayer.GetComponent<PlayerMovement>().enabled = true;
        GetComponent<LevelManager>().clear();
        GetComponent<LevelManager>().SetUp();
        Time.timeScale = 1;
        currentState = GameState.Playing;   
    }

    /// <summary>
    /// Ends the game;
    /// </summary>
    public void EndGame()
    {
        LeftPlayer.GetComponent<PlayerMovement>().enabled = false;
        RightPlayer.GetComponent<PlayerMovement>().enabled = false;
        currentState = GameState.Dead;
        Time.timeScale = 0;
        if (checkHighscore((int)TotalDistance))
            saveHighscore((int)TotalDistance);
        Highscore.text="Highscore: " + PlayerPrefs.GetInt("highscore");
    }

    public  void ResumeGame()
    {
        Time.timeScale = 1;
        PauseScreen.SetActive(false);
        currentState = GameState.Playing;

    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseScreen.SetActive(true);
        currentState = GameState.Paused;
    }

    public void ControlsGame()
    {
        if (controlsPanel.activeSelf == false)
        {

            Time.timeScale = 0;
            controlsPanel.SetActive(true);
            currentState = GameState.Controls;
        }
        else if (controlsPanel.activeSelf == true)
        {
            Time.timeScale = 1;
            controlsPanel.SetActive(false);
            currentState = GameState.Playing;

        }
    }



    float CalculateDistanceTraveled()
    {
        float AvrageTravleDistance = (LeftPlayer.transform.position.y + RightPlayer.transform.position.y) / 2;
        return AvrageTravleDistance;
    }

    public enum GameState
    {
        Playing,
        Paused,
        Dead,
        Controls
    }

    public bool checkHighscore(int myScore)
    {
        bool haskey = PlayerPrefs.HasKey("highscore");
        int highscore = 0;
        if (haskey)
        {
            highscore = PlayerPrefs.GetInt("highscore");
            if (highscore < myScore)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return true; 
        }
    }

    public void saveHighscore(int myScore)
    {
        PlayerPrefs.SetInt("highscore", myScore);
        PlayerPrefs.Save();
    }
}
