﻿using UnityEngine;
using System.Collections;

public class LevelScroll : MonoBehaviour {

    public float scrollSpeed;
	// Use this for initialization
	void Update()
    {
        transform.Translate(Vector3.down * Time.deltaTime * scrollSpeed);
    }
}
