﻿using UnityEngine;
using System.Collections;

public class AudioScript : MonoBehaviour {

    public AudioClip aoBreak;
    public AudioClip aoDash;
    public AudioClip aoScream;
    public AudioClip aoSpark;
    AudioSource audio;


    // Use this for initialization
    void Start () {

        audio = GetComponent<AudioSource>();

	}
       
    public void playDashClip()
    {
        audio.PlayOneShot(aoDash);
    }

    public void playBreakClip()
    {
        audio.PlayOneShot(aoBreak);
    }

    public void playSparkClip()
    {
        audio.PlayOneShot(aoSpark);
    }

    public void playScreamClip()
    {
        audio.PlayOneShot(aoScream);
    }

}

